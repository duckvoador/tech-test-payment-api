using Microsoft.AspNetCore.Mvc;


using tech_test_payment_api.src.Models;
using tech_test_payment_api.src.Persistence;

namespace tech_test_payment_api.src.Controllers;
[ApiController]
[Route("[controller]")]
 public class VendaController : ControllerBase
    {
        private  VendaContext   _context { get; set; }
        public VendaController(VendaContext context){
        this._context = context;
        }
      
    [HttpPost("CadastroVendedor")]
    public Vendedor Post(Vendedor vendedor)
    {
        _context.Vendedores.Add(vendedor);
        _context.SaveChanges();
        return vendedor;

    }
    [HttpPost("CadastroProdutos")]
    public Produto CadastrarProduto(Produto produto)
    {
        _context.Produtos.Add(produto);
        _context.SaveChanges();
        return produto;

    }
    [HttpGet("ListarVendedores")]
    public ActionResult< List<Vendedor>> ObterVendedores(){
       

       var result =_context.Vendedores.ToList();
       if(!result.Any())return NoContent();
       
       return Ok(result);
     
    }
    [HttpGet("ListProdutos")]
    public ActionResult< List<Produto>> ListarProdutos(){
       

       var result =_context.Produtos.ToList();
       if(!result.Any())return NoContent();
       
       return Ok(result);
     
    }
      [HttpPost]
    public Venda Venda(Venda venda)
    {
    
        _context.Vendas.Add(venda);
        _context.SaveChanges();
        return venda;

    }

   

    [HttpGet("ListarVendas")]
    public ActionResult< List<Venda>> Get()
    {
        var result =_context.Vendas.ToList(); 
        if(!result.Any())return NoContent();
       
        return Ok(result);
     }

    [HttpGet("Venda{id}")]
    public IActionResult ObterPorId(int id)
    {
        var venda = _context.Vendas.Find(id);
        if (venda == null)
                return NotFound();
        
        return Ok(venda);
    }

    [HttpPut("AtualizarSatatusVendaPositivo{id}")]
        public IActionResult AtualizarVendaPositivo(int id)
        {
            var statusVenda = _context.Vendas.Find(id);

            if (statusVenda == null)
                return NotFound();
            if (statusVenda.Status =="Aguardando pagamento")
                statusVenda.Status = "Pagamento Aprovado";
            
                
            else if(statusVenda.Status =="Pagamento Aprovado")
                statusVenda.Status = "Enviado para Transportadora";
            
            else if(statusVenda.Status == "Enviado para Transportadora")
                statusVenda.Status = "Entregue";

            _context.Vendas.Update(statusVenda); 
            _context.SaveChanges();  

            return Ok(statusVenda);
        }
        [HttpPut("AtualizarSatatusVendaNegativo{id}")]
        public IActionResult AtualizarVendaNegativo(int id)
        {
            var statusVenda = _context.Vendas.Find(id);

            if (statusVenda == null)
                return NotFound();
            if (statusVenda.Status =="Aguardando pagamento")
                statusVenda.Status = "Cancelada";
            
                
            else if(statusVenda.Status =="Pagamento Aprovado")
                statusVenda.Status = "Cancelada";
            
            

            _context.Vendas.Update(statusVenda); 
            _context.SaveChanges();  

            return Ok(statusVenda);
            }
    }
