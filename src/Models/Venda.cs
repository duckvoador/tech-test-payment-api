
namespace tech_test_payment_api.src.Models
{
   
    public class Venda
    {  
        public int IdVenda {get;set;}
        public string Data{get;set;}
        
        public Vendedor Vendedor{get;set;}
        public ICollection<Produto> Produtos{get;set;}
        public string Status {get;set;} = "Aguardando pagamento";
     
     }

}