
namespace tech_test_payment_api.src.Models
{
    public class Vendedor 
    {
        public Vendedor(string Nome, int Idade, int Cpf, string Email)
        {
            this.Nome = Nome;
            this.Idade = Idade;
            this.Email = Email;
            this.Cpf = Cpf;
        }
        public int IdVendedor {get; set;}
        public string Nome {get; set;}
        public int Idade {get; set;}
        public int Cpf {get; set;}
        public string Email {get; set;}
       
        

    }
}