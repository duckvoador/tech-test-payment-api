using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.src.Models;

namespace tech_test_payment_api.src.Persistence
{
    public class VendaContext : DbContext
    {
        public VendaContext (DbContextOptions<VendaContext>options): base(options)
      {

      }  
      public DbSet<Venda> Vendas{get; set;}
      public DbSet<Vendedor> Vendedores {get; set;}
      public DbSet<Produto> Produtos {get; set;}

    protected  override void OnModelCreating(ModelBuilder buldier){

    buldier.Entity<Venda>(table =>{
        table.HasKey (e => e.IdVenda);
       
        
    });
    
    buldier.Entity<Vendedor>( table =>{
        table.HasKey(e => e.IdVendedor);
        
    });
     buldier.Entity<Produto>( table =>{
        table.HasKey(e => e.IdProduto);
        
        
    });

    }
    
    }

}